package org.kvanwani.internship;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

/**
 * Hello world!
 *
 */
public class App {
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static HttpTransport HTTP_TRANSPORT;

	private static final String APPLICATION_NAME = "Internship Assignmnet";

	private static String client_id;
	private static String client_secret;
	private static String refresh_token;
	private static String file_location;

	public static Drive getDriveService() throws IOException {
		Credential credential = authorize();
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	public static Credential authorize() throws IOException {

		String accessToken = refreshAccessToken(refresh_token, client_id, client_secret);
		GoogleCredential credentials = new GoogleCredential.Builder().setClientSecrets(client_id, client_secret)
				.setJsonFactory(JSON_FACTORY).setTransport(HTTP_TRANSPORT).build().setRefreshToken(refresh_token)
				.setAccessToken(accessToken);

		return credentials;
	}

	static String refreshAccessToken(String refreshToken, String clientId, String clientSecret) throws IOException {
			TokenResponse response = new GoogleRefreshTokenRequest(HTTP_TRANSPORT, JSON_FACTORY, refreshToken, clientId,
					clientSecret).execute();
			System.out.println("Access token: " + response.getAccessToken());
			return response.getAccessToken();
	}

	public static void loadProperties() {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = App.class.getResourceAsStream("/config.properties");

			
			prop.load(input);
			
			System.out.println(prop.getProperty("client_secret"));
			client_id = prop.getProperty("client_id");
			client_secret = prop.getProperty("client_secret");
			refresh_token = prop.getProperty("refresh_token");
			file_location = prop.getProperty("file_location");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static String getFileName(String fileocation){
		String[] str_array = fileocation.split("/");
		int stringLength = str_array.length;
		return str_array[stringLength-1];
	}
	public static void uploadFile() throws IOException {

		Drive service = getDriveService();
		File body = new File();
		String fileName = getFileName(file_location);
		body.setName(fileName);
		body.setDescription("A test document");
		body.setMimeType("text/plain");
		java.io.File fileContent = new java.io.File(file_location);
		FileContent mediaContent = new FileContent(null, fileContent);
		File file = service.files().create(body, mediaContent).execute();
		System.out.println("File uploaded - File id is : " + file.getId());

	}
}
